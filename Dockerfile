FROM python:3.12.0b3-alpine3.18
RUN apk --no-cache add git
RUN git clone https://github.com/tornadoweb/tornado.git
WORKDIR /tornado

COPY tornado.py /tornado/
CMD ["python", "tornado.py"]
EXPOSE 8888/tcp

